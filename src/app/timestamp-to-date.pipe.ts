import { Pipe, PipeTransform } from '@angular/core';
/*
 * Convert time stamp to string date
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | timestampToDate
 */
@Pipe({ name: 'timestampToDate' })
export class TimeStampToDatePipe implements PipeTransform {
  transform(value: number): Date {
    return new Date(value * 1000);
  }
}
