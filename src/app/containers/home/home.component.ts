import { Component, OnInit } from '@angular/core';
import { Card } from '../../entities/card';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor() {}
  cards: Card[] = [];
  removeCard = (id): void => {
    this.cards = this.cards.filter((card) => (card.id === id ? null : card));
  };
  createdCard = (card: Card): void => {
    this.cards.push(card);
  };

  ngOnInit(): void {
    for (let i = 0; i < 10; i++)
      this.cards.push(
        new Card(
          'sample title',
          'sample description',
          Date.now() / 1000 + 360 * Math.ceil(Math.random() * 180)
        )
      );
  }
}
