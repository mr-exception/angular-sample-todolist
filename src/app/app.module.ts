import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './elements/card/card.component';
import { HomeComponent } from './containers/home/home.component';
import { TimeStampToDatePipe } from './timestamp-to-date.pipe';
import { ButtonComponent } from './elements/button/button.component';
import { CreateCardComponent } from './elements/create-card/create-card.component';
import { InputComponent } from './elements/input/input.component';
@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    HomeComponent,
    TimeStampToDatePipe,
    ButtonComponent,
    CreateCardComponent,
    InputComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
