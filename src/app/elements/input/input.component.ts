import { Component, OnInit, Input, forwardRef } from '@angular/core';
import {
  FormControl,
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  FormGroup,
} from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true,
    },
  ],
})
export class InputComponent implements OnInit, ControlValueAccessor {
  @Input() label: string;
  @Input() parentForm: FormGroup;
  @Input() control: string;
  constructor() {}
  hasError(error: string): boolean {
    const formControl = this.parentForm.get(this.control);
    if (formControl.invalid && (formControl.dirty || formControl.touched))
      return formControl.hasError(error);
    else return false;
  }
  writeValue(value: string): void {
    console.log(value);
  }
  propagateChange = (_: any) => {};
  registerOnChange(fn: any): void {
    console.log(fn);
  }
  registerOnTouched(fn: any): void {}

  ngOnInit(): void {}
}
