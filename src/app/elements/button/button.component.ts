import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css'],
})
export class ButtonComponent implements OnInit {
  @Input() onClick: () => {};
  @Input() border: string = 'solid';
  @Input() title: string = 'click!';
  @Input() disabled: boolean = false;
  @Input() type: string = 'button';

  constructor() {}

  ngOnInit(): void {}
}
