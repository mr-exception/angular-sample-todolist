import { Component, OnInit, Input } from '@angular/core';
import { Card } from '../../entities/card';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  @Input() card: Card;
  @Input() removeWithId: (id: number) => {};

  remove = (): void => {
    this.removeWithId(this.card.id);
  };

  constructor() {}

  ngOnInit(): void {}
}
