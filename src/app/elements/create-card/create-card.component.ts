import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Card } from 'src/app/entities/card';

@Component({
  selector: 'app-create-card',
  templateUrl: './create-card.component.html',
  styleUrls: ['./create-card.component.css'],
})
export class CreateCardComponent implements OnInit {
  @Input() created: (card: Card) => {};
  form = new FormGroup({
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    due_date: new FormGroup({
      year: new FormControl('', [Validators.required]),
      month: new FormControl('', [Validators.required]),
      day: new FormControl('', [Validators.required]),
      hour: new FormControl('', [Validators.required]),
      minute: new FormControl('', [Validators.required]),
    }),
  });
  constructor() {}
  onSubmit(): void {
    const title = this.form.get('title').value;
    const description = this.form.get('title').value;
    const due_date = new Date(
      this.form.get('due_date.year').value,
      this.form.get('due_date.month').value - 1,
      this.form.get('due_date.day').value,
      this.form.get('due_date.hour').value,
      this.form.get('due_date.minute').value
    );
    const card = new Card(title, description, due_date.getTime() / 1000);
    this.created(card);
  }
  ngOnInit(): void {}
}
