export class Card {
  id: number;

  constructor(
    public title: string,
    public description: string,
    public due_date: number,
    public is_done: boolean = false
  ) {
    this.id = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
  }
  setDone(): void {
    this.is_done = true;
  }
  setDoing(): void {
    this.is_done = false;
  }
}
